package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model

class MostWantedPersonBriefUiModel(
    val uid: String,
    val id: String,

    val images: List<ImageUiModel>,

    val title: String,
    val description: String,
    val status: StatusUiModel,

    val modifiedDaysAgo: Int,

    val subjects: List<String>
)
