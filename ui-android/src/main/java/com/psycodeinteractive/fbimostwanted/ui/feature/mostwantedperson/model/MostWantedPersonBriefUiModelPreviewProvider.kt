package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model

import androidx.compose.ui.tooling.preview.PreviewParameterProvider

class MostWantedPersonBriefUiModelPreviewProvider : PreviewParameterProvider<List<MostWantedPersonBriefUiModel>> {
    private val person1 = MostWantedPersonBriefUiModel(
        uid = "1",
        id = "1",
        images = listOf(
            ImageUiModel(
                original = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                large = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                thumb = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                caption = "https://www.gstatic.com/webp/gallery3/1.sm.png"
            )
        ),
        title = "Person 1",
        description = "Person 1 is dangerous",
        status = StatusUiModel.NotAvailable,
        modifiedDaysAgo = 50,
        subjects = listOf("Kidnapping", "GTA")
    )

    private val person2 = MostWantedPersonBriefUiModel(
        uid = "2",
        id = "2",
        images = listOf(
            ImageUiModel(
                original = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                large = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                thumb = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                caption = "https://www.gstatic.com/webp/gallery/4.sm.jpg"
            )
        ),
        title = "Person 2",
        description = "Person 2 is dangerous",
        status = StatusUiModel.NotAvailable,
        modifiedDaysAgo = 40,
        subjects = listOf("Larceny")
    )

    private val person3 = MostWantedPersonBriefUiModel(
        uid = "3",
        id = "3",
        images = listOf(
            ImageUiModel(
                original = "https://www.gstatic.com/webp/gallery/1.sm.jpg",
                large = "https://www.gstatic.com/webp/gallery/1.sm.jpg",
                thumb = "https://www.gstatic.com/webp/gallery/1.sm.jpg",
                caption = "https://www.gstatic.com/webp/gallery/1.sm.jpg"
            )
        ),
        title = "Person 3",
        description = "Person 3 is dangerous",
        status = StatusUiModel.NotAvailable,
        modifiedDaysAgo = 10,
        subjects = listOf("White collar crime")
    )

    override val values: Sequence<List<MostWantedPersonBriefUiModel>> = sequenceOf(
        emptyList(),
        listOf(person1),
        listOf(person1, person2),
        listOf(person1, person2, person3),
        listOf(person2, person3)
    )
}
