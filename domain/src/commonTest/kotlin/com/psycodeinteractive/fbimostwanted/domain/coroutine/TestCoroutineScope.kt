package com.psycodeinteractive.fbimostwanted.domain.coroutine

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.setMain

object TestCoroutineScope {
    fun createTestScope() = TestScope().apply {
        val testDispatcher = StandardTestDispatcher(testScheduler)
        Dispatchers.setMain(testDispatcher)
    }
}
