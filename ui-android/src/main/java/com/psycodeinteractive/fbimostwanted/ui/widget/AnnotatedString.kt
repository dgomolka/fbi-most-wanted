package com.psycodeinteractive.fbimostwanted.ui.widget

import android.graphics.Typeface.BOLD
import android.graphics.Typeface.BOLD_ITALIC
import android.graphics.Typeface.ITALIC
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle.Companion.Italic
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.style.TextDecoration.Companion.Underline
import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY

@Composable
fun annotatedString(html: String): AnnotatedString {
    val spanned = remember(html) {
        HtmlCompat.fromHtml(html, FROM_HTML_MODE_LEGACY)
    }
    return remember(spanned) {
        buildAnnotatedString {
            append(spanned.toString())
            spanned.getSpans(0, spanned.length, Any::class.java).forEach { span ->
                val start = spanned.getSpanStart(span)
                val end = spanned.getSpanEnd(span)
                when (span) {
                    is StyleSpan -> when (span.style) {
                        BOLD -> addStyle(SpanStyle(fontWeight = Bold), start, end)
                        ITALIC -> addStyle(SpanStyle(fontStyle = Italic), start, end)
                        BOLD_ITALIC -> addStyle(
                            SpanStyle(
                                fontWeight = Bold,
                                fontStyle = Italic
                            ),
                            start,
                            end
                        )
                    }
                    is UnderlineSpan ->
                        addStyle(SpanStyle(textDecoration = Underline), start, end)
                    is ForegroundColorSpan ->
                        addStyle(SpanStyle(color = Color(span.foregroundColor)), start, end)
                }
            }
        }
    }
}
