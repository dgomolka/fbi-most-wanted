package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle.State.CREATED
import com.psycodeinteractive.fbimostwanted.presentation.feature.mostwantedperson.MostWantedPersonViewModel
import com.psycodeinteractive.fbimostwanted.ui.FBIMostWantedTheme
import com.psycodeinteractive.fbimostwanted.ui.R
import com.psycodeinteractive.fbimostwanted.ui.annotation.AllModesPreview
import com.psycodeinteractive.fbimostwanted.ui.extension.value
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.mapper.MostWantedPersonPresentationToUiMapper
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.mapper.MostWantedPersonScreenPresentationDestinationToNavigationCallbackMapper
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.ImageUiModel
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.MostWantedPersonTopBarResourcesUiModel
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.MostWantedPersonUiModel
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.MostWantedPersonUiModelPreviewProvider
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.SexUiModel
import com.psycodeinteractive.fbimostwanted.ui.screen.OnLifecycle
import com.psycodeinteractive.fbimostwanted.ui.screen.Screen
import com.psycodeinteractive.fbimostwanted.ui.screen.ScreenNavigationContainer
import com.psycodeinteractive.fbimostwanted.ui.widget.LabeledChip
import com.psycodeinteractive.fbimostwanted.ui.widget.annotatedString
import com.psycodeinteractive.fbimostwanted.ui.widget.chipModifier
import com.psycodeinteractive.fbimostwanted.ui.widget.image.HorizontalImageGallery
import com.psycodeinteractive.fbimostwanted.ui.widget.topbar.TopBar
import com.psycodeinteractive.fbimostwanted.ui.widget.topbar.model.BackNavigationTypeUiModel
import com.ramcosta.composedestinations.annotation.Destination
import me.tatarka.inject.annotations.Inject

typealias MostWantedPersonScreen = @Composable (
    screenNavigationCallbacks: MostWantedPersonScreenNavigationCallbacks,
    personId: String
) -> Unit

@Destination
@Inject
@Composable
fun MostWantedPersonScreen(
    provideMostWantedPersonViewModel: () -> MostWantedPersonViewModel,
    mostWantedPersonPresentationToUiMapper: MostWantedPersonPresentationToUiMapper,
    presentationDestinationToNavigationCallbackMapper: MostWantedPersonScreenPresentationDestinationToNavigationCallbackMapper,
    screenNavigationCallbacks: MostWantedPersonScreenNavigationCallbacks,
    personId: String
) {
    Screen(
        provideViewModel = provideMostWantedPersonViewModel,
        screenNavigationContainer = ScreenNavigationContainer(
            screenNavigationCallbacks,
            presentationDestinationToNavigationCallbackMapper
        )
    ) { viewModel, viewState, _ ->
        OnLifecycle(minActiveState = CREATED) {
            viewModel.onViewCreated(personId)
        }

        val mostWantedPerson = viewState.mostWantedPerson?.let(mostWantedPersonPresentationToUiMapper::toUi)

        MostWantedPersonScreenContent(mostWantedPerson) {
            viewModel.onCloseAction()
        }
    }
}

@Composable
private fun MostWantedPersonScreenContent(
    mostWantedPerson: MostWantedPersonUiModel?,
    onBackNavigationClick: (BackNavigationTypeUiModel) -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        TopBar(
            resources = MostWantedPersonTopBarResourcesUiModel,
            onBackNavigationClick = onBackNavigationClick
        )
        Divider(
            color = topDividerColor
        )
        MostWantedPersonDetails(mostWantedPerson)
    }
}

@Composable
private fun MostWantedPersonDetails(mostWantedPerson: MostWantedPersonUiModel?) {
    mostWantedPerson?.run {
        val scrollState = rememberScrollState()
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.spacedBy(columnItemsSpacing)
        ) {
            Title(title)
            Gallery(images)
            Sex(sex)
            Nationality(nationality)
            Occupations(occupations)
            Subjects(subjects)
            Text(text = annotatedString(html = caution))
            Text(text = annotatedString(html = details))
        }
    }
}

@Composable
private fun Title(title: String) {
    Text(
        modifier = Modifier.padding(top = 4.dp),
        text = title,
        style = MaterialTheme.typography.h6
    )
}

@Composable
private fun Gallery(images: List<ImageUiModel>) {
    if (images.isNotEmpty()) {
        HorizontalImageGallery(
            modifier = Modifier.height(galleryHeight),
            urls = images.map(ImageUiModel::large)
        )
    }
}

@Composable
private fun Sex(sex: SexUiModel) {
    LabeledChip(
        labelText = R.string.sex.value,
        valueText = sex.stringResource.value
    )
}

@Composable
private fun Nationality(nationality: String) {
    if (nationality.isNotEmpty()) {
        LabeledChip(
            labelText = R.string.nationality.value,
            valueText = nationality
        )
    }
}

@Composable
private fun Occupations(occupations: List<String>) {
    if (occupations.isNotEmpty()) {
        val occupationsText = occupations.joinToString(separator = ",")
        LabeledChip(
            labelText = R.string.occupations.value,
            valueText = occupationsText
        )
    }
}

@Composable
private fun Subjects(subjects: List<String>) {
    if (subjects.isNotEmpty()) {
        LazyRow(
            horizontalArrangement = Arrangement.spacedBy(subjectChipSpacing)
        ) {
            items(subjects) { subject ->
                Text(
                    modifier = Modifier.chipModifier(subjectChipColorDark, chipShape),
                    text = subject,
                    style = MaterialTheme.typography.subtitle2
                )
            }
        }
    }
}

@Composable
@AllModesPreview
private fun MostWantedPersonScreenContentPreview(
    @PreviewParameter(MostWantedPersonUiModelPreviewProvider::class) mostWantedPerson: MostWantedPersonUiModel
) {
    FBIMostWantedTheme {
        MostWantedPersonScreenContent(
            mostWantedPerson = mostWantedPerson,
            onBackNavigationClick = {}
        )
    }
}

private val galleryHeight = 200.dp
private val columnItemsSpacing = 10.dp
private val topDividerColor = Color.LightGray.copy(0.35f)
private val subjectChipColorDark = Color.DarkGray.copy(1f)
private val chipShape = RoundedCornerShape(6.dp)
private val subjectChipSpacing = 10.dp
