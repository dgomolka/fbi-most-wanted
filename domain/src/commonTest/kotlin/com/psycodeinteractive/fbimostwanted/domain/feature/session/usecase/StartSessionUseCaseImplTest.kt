package com.psycodeinteractive.fbimostwanted.domain.feature.session.usecase

import com.psycodeinteractive.fbimostwanted.domain.coroutine.TestCoroutineScope
import com.psycodeinteractive.fbimostwanted.domain.coroutine.testCoroutineContextProvider
import com.psycodeinteractive.fbimostwanted.domain.feature.session.repository.AppSessionRepository
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.given
import io.mockative.mock
import io.mockative.once
import io.mockative.thenDoNothing
import io.mockative.verify
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import kotlin.test.BeforeTest
import kotlin.test.Test

class StartSessionUseCaseImplTest {

    @Mock
    val appSessionRepository = mock(classOf<AppSessionRepository>())

    private lateinit var testScope: TestScope

    private lateinit var classUnderTest: StartSessionUseCaseImpl

    @BeforeTest
    fun setup() {
        testScope = TestCoroutineScope.createTestScope()
        classUnderTest = StartSessionUseCaseImpl(appSessionRepository, testCoroutineContextProvider)
    }

    @Test
    fun `When executeInBackground Then verify startSession was called`() = testScope.runTest {
        // Given
        given(appSessionRepository).coroutine { startSession() }.thenDoNothing()

        // When
        classUnderTest.executeInBackground(Unit, testScope)

        // Then
        verify(appSessionRepository).coroutine { startSession() }.wasInvoked(once)
    }
}
