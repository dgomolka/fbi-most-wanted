package com.psycodeinteractive.fbimostwanted.domain.coroutine

import kotlin.coroutines.CoroutineContext

private class TestCoroutineContextProvider(
    override val main: CoroutineContext = TestCoroutineScope.createTestScope().coroutineContext,
    override val io: CoroutineContext = TestCoroutineScope.createTestScope().coroutineContext
) : CoroutineContextProvider

val testCoroutineContextProvider: CoroutineContextProvider =
    TestCoroutineContextProvider()
