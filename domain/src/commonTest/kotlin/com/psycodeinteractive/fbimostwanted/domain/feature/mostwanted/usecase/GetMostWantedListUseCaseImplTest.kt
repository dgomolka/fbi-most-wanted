package com.psycodeinteractive.fbimostwanted.domain.feature.mostwanted.usecase

import com.psycodeinteractive.fbimostwanted.domain.coroutine.testCoroutineContextProvider
import com.psycodeinteractive.fbimostwanted.domain.feature.mostwanted.model.givenMostWantedList
import com.psycodeinteractive.fbimostwanted.domain.feature.mostwanted.repository.MostWantedRepository
import io.mockative.Mock
import io.mockative.classOf
import io.mockative.given
import io.mockative.mock
import io.mockative.once
import io.mockative.verify
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class GetMostWantedListUseCaseImplTest {

    @Mock
    val mostWantedRepository = mock(classOf<MostWantedRepository>())

    private lateinit var testScope: TestScope

    private lateinit var classUnderTest: GetMostWantedListUseCaseImpl

    @BeforeTest
    fun setup() {
        classUnderTest = GetMostWantedListUseCaseImpl(mostWantedRepository, testCoroutineContextProvider)
    }

    @Test
    fun `When executeInBackground Then verify most wanted list was requested and returned`() = testScope.runTest {
        // Given
        given(mostWantedRepository).coroutine { getMostWantedList() }.thenReturn(givenMostWantedList)

        // When
        val actualResult = classUnderTest.executeInBackground(Unit, testScope)

        // Then
        verify(mostWantedRepository).coroutine { getMostWantedList() }.wasInvoked(once)
        assertEquals(givenMostWantedList, actualResult)
    }
}
