package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.mapper

import com.psycodeinteractive.fbimostwanted.presentation.feature.mostwantedperson.model.MostWantedPersonPresentationModel
import com.psycodeinteractive.fbimostwanted.ui.contract.mapper.PresentationToUiMapper
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model.MostWantedPersonBriefUiModel

class MostWantedPersonPresentationToBriefUiMapper(
    private val imageDomainToPresentationMapper: ImagePresentationToUiMapper,
    private val statusDomainToPresentationMapper: StatusPresentationToUiMapper
) : PresentationToUiMapper<MostWantedPersonPresentationModel, MostWantedPersonBriefUiModel>() {
    override fun map(input: MostWantedPersonPresentationModel) = MostWantedPersonBriefUiModel(
        uid = input.uid,
        id = input.id,
        images = input.images.map(imageDomainToPresentationMapper::toUi),

        title = input.title,
        description = input.description,
        status = statusDomainToPresentationMapper.toUi(input.status),
        modifiedDaysAgo = input.modifiedDaysAgo,

        subjects = input.subjects
    )
}
