package com.psycodeinteractive.fbimostwanted.ui.widget

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun LabeledChip(
    modifier: Modifier = Modifier,
    labelText: String,
    valueText: String
) {
    Row(
        modifier = modifier.clip(RoundedCornerShape(6.dp))
    ) {
        Text(
            modifier = Modifier.chipModifier(chipColorLight),
            text = labelText,
            style = MaterialTheme.typography.subtitle2
        )
        Text(
            modifier = Modifier.chipModifier(chipColorDark),
            text = valueText,
            style = MaterialTheme.typography.subtitle2
        )
    }
}

fun Modifier.chipModifier(
    color: Color,
    shape: RoundedCornerShape = RoundedCornerShape(0.dp)
) = background(color, shape)
    .padding(horizontal = 6.dp, vertical = 4.dp)

private val chipColorLight = Color.Gray.copy(0.35f)
private val chipColorDark = Color.Gray.copy(0.8f)
