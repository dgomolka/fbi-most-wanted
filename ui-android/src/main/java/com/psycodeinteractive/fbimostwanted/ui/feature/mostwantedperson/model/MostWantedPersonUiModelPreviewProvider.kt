package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model

import androidx.compose.ui.tooling.preview.PreviewParameterProvider

class MostWantedPersonUiModelPreviewProvider : PreviewParameterProvider<MostWantedPersonUiModel> {
    private val person1 = MostWantedPersonUiModel(
        uid = "1",
        id = "1",
        rewardText = "reward",
        aliases = listOf("alias1", "alias2"),
        publication = "publication",
        url = "url",
        warningMessage = "warningMessage",
        details = "details",
        occupations = listOf("occupation1", "occupdation2"),
        nationality = "nationality",
        placeOfBirth = "placeOfBirth",
        datesOfBirthUsed = listOf("1", "2"),
        languages = listOf("pl", "en"),
        sex = SexUiModel.Male,
        scarsAndMarks = "scarsAndMarks",
        complexion = "complexion",
        race = "race",
        raceRaw = "raceRaw",
        build = "build",
        hair = "hair",
        hairRaw = "hairRaw",
        eyes = "eyes",
        eyesRaw = "eyesRaw",
        ageMin = 20,
        ageMax = 50,
        ageRange = "20-50",
        weight = "weight",
        weightMin = 35,
        weightMax = 200,
        heightMin = 40,
        heightMax = 250,
        rewardMin = 0,
        rewardMax = 100000000,
        files = emptyList(),
        images = listOf(
            ImageUiModel(
                original = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                large = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                thumb = "https://www.gstatic.com/webp/gallery3/1.sm.png",
                caption = "https://www.gstatic.com/webp/gallery3/1.sm.png"
            )
        ),
        title = "Title",
        description = "Description",
        status = StatusUiModel.Recovered,
        remarks = "remarks",
        path = "path",
        possibleStates = listOf("CA", "NY"),
        possibleCountries = listOf("USA", "PL"),
        caution = "caution",
        modifiedDaysAgo = 20,
        subjects = listOf("murder", "gta"),
        ncic = "ncic",
        fieldOffices = emptyList()
    )

    val person2 = MostWantedPersonUiModel(
        uid = "2",
        id = "2",
        rewardText = "reward",
        aliases = listOf("alias1", "alias2"),
        publication = "publication",
        url = "url",
        warningMessage = "warningMessage",
        details = "details",
        occupations = listOf("occupation1", "occupdation2"),
        nationality = "nationality",
        placeOfBirth = "placeOfBirth",
        datesOfBirthUsed = listOf("1", "2"),
        languages = listOf("pl", "en"),
        sex = SexUiModel.Female,
        scarsAndMarks = "scarsAndMarks",
        complexion = "complexion",
        race = "race",
        raceRaw = "raceRaw",
        build = "build",
        hair = "hair",
        hairRaw = "hairRaw",
        eyes = "eyes",
        eyesRaw = "eyesRaw",
        ageMin = 30,
        ageMax = 40,
        ageRange = "30-40",
        weight = "weight",
        weightMin = 35,
        weightMax = 200,
        heightMin = 40,
        heightMax = 250,
        rewardMin = 0,
        rewardMax = 100000000,
        files = emptyList(),
        images = listOf(
            ImageUiModel(
                original = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                large = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                thumb = "https://www.gstatic.com/webp/gallery/4.sm.jpg",
                caption = "https://www.gstatic.com/webp/gallery/4.sm.jpg"
            )
        ),
        title = "Title",
        description = "Description",
        status = StatusUiModel.Captured,
        remarks = "remarks",
        path = "path",
        possibleStates = listOf("OH", "TX"),
        possibleCountries = listOf("USA", "PL"),
        caution = "caution",
        modifiedDaysAgo = 20,
        subjects = listOf("murder", "gta"),
        ncic = "ncic",
        fieldOffices = emptyList()
    )

    override val values: Sequence<MostWantedPersonUiModel> = sequenceOf(
        person1,
        person2
    )
}
