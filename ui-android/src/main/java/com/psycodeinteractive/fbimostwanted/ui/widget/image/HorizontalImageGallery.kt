package com.psycodeinteractive.fbimostwanted.ui.widget.image

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale.Companion.FillHeight
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.request.CachePolicy.ENABLED
import coil.request.ImageRequest
import com.psycodeinteractive.fbimostwanted.ui.R
import com.psycodeinteractive.fbimostwanted.ui.widget.FBIMostWantedLazyRow
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.coil.CoilImage

@Composable
fun HorizontalImageGallery(
    modifier: Modifier = Modifier,
    urls: List<String>
) {
    FBIMostWantedLazyRow(
        modifier = modifier.fillMaxHeight(),
        items = urls
    ) { url ->
        val context = LocalContext.current
        CoilImage(
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(imageCornerRadius)),
            imageRequest = {
                ImageRequest.Builder(context)
                    .data(url)
                    .crossfade(true)
                    .build()
            },
            imageLoader = {
                ImageLoader.Builder(LocalContext.current)
                    .memoryCachePolicy(ENABLED)
                    .crossfade(true)
                    .build()
            },
            imageOptions = ImageOptions(
                contentScale = FillHeight
            ),
            loading = {
                ImageLoading()
            },
            failure = {
                ImageFetchFailure()
            }
        )
    }
}

@Composable
private fun BoxScope.ImageFetchFailure() {
    Image(
        modifier = Modifier
            .fillMaxHeight()
            .aspectRatio(ratio = 1.0f, matchHeightConstraintsFirst = true)
            .size(imageFetchStateIndicatorsSize)
            .align(Center),
        painter = painterResource(id = R.drawable.image_broken),
        contentDescription = "ImageFetchFailure",
        colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface)
    )
}

@Composable
private fun BoxScope.ImageLoading() {
    CircularProgressIndicator(
        modifier = Modifier
            .aspectRatio(ratio = 1.0f, matchHeightConstraintsFirst = true)
            .size(imageFetchStateIndicatorsSize)
            .align(Center),
        color = MaterialTheme.colors.onSurface
    )
}

private val imageFetchStateIndicatorsSize = 60.dp
private val imageCornerRadius = 12.dp
