package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.mapper

import com.psycodeinteractive.fbimostwanted.presentation.destination.BackDestination
import com.psycodeinteractive.fbimostwanted.presentation.navigation.PresentationDestination
import com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.MostWantedPersonScreenNavigationCallbacks
import com.psycodeinteractive.fbimostwanted.ui.screen.PresentationDestinationToNavigationCallbackMapper

class MostWantedPersonScreenPresentationDestinationToNavigationCallbackMapper :
    PresentationDestinationToNavigationCallbackMapper<MostWantedPersonScreenNavigationCallbacks> {
    override fun map(
        destination: PresentationDestination,
        screenNavigationCallbacks: MostWantedPersonScreenNavigationCallbacks
    ) = when (destination) {
        is BackDestination -> screenNavigationCallbacks.onCloseScreen()
        else -> {}
    }
}
