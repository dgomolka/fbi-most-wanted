package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson.model

import com.psycodeinteractive.fbimostwanted.ui.R
import com.psycodeinteractive.fbimostwanted.ui.widget.topbar.model.BackNavigationTypeUiModel
import com.psycodeinteractive.fbimostwanted.ui.widget.topbar.model.TopBarResourcesUiModel

object MostWantedPersonTopBarResourcesUiModel : TopBarResourcesUiModel(
    titleSmallTextResource = R.string.wanted_person_details,
    backNavigationType = BackNavigationTypeUiModel.Close
)
