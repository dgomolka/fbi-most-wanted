package com.psycodeinteractive.fbimostwanted.ui.feature.mostwantedperson

import com.psycodeinteractive.fbimostwanted.ui.screen.ScreenNavigationCallbacks

data class MostWantedPersonScreenNavigationCallbacks(
    val onCloseScreen: () -> Unit
) : ScreenNavigationCallbacks
